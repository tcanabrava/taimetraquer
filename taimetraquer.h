
#ifndef TAIMETRAQUER_H
#define TAIMETRAQUER_H

#include <plasma/abstractrunner.h>
#include <KIcon>
#include <QTimer>
#include <QDateTime>
#include <QMap>
#include <QDir>
#include <QFile>

// Define our plasma Runner
struct TaimeTraquerInfo{
  /* Work is a normal entry in the gtimelog file.
   * NoWork is amn entry that ends with '**'
   * Hidden is an entry that ends with '***'
   * Arriving is an entry just by 'arrived' */
  
  enum eventType{ Work, NotWork, Arriving, Hidden};
  QDate   m_date;
  QTime   m_time;
  QString m_event;
  QString m_comments;
  uchar   m_eventType;
};

class TaimeTraquer : public Plasma::AbstractRunner {
    Q_OBJECT

public:
  enum RunnerActions{ ARRIVED, RECORD, NOTHING, INFO };
  
    // Basic Create/Destroy
    TaimeTraquer( QObject *parent, const QVariantList& args );
    ~TaimeTraquer();

    void match(Plasma::RunnerContext &context);
    void run(const Plasma::RunnerContext &context, const Plasma::QueryMatch &match);
    void reloadConviguration();
    void createRunOptions(QWidget *widget);
    void createCache();
    void record(const QString& data);
    TaimeTraquerInfo createInfo(QDate date, QTime time, const QString& data);
    
private slots:
  void init();
  void prepareForMatchSession();
  void matchSessionFinished();
  void clearCache();
  
private:
  QString m_trigger;
  QTimer m_deleteCacheTimer;
  QMap<QString, TaimeTraquerInfo> m_infoMap;
  QDir m_gtimelogDir;
  QFile m_gtimelogLog;
};

// This is the command that links your applet to the .desktop file
K_EXPORT_PLASMA_RUNNER(taimetraquer, TaimeTraquer)

#endif
