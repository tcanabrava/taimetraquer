#include "taimetraquer.h"

#include <KDebug>
#include <KIcon>
#include <KMessageBox>
#include <KMimeType>
#include <QDir>
#include <QTime>
#include <QDate>

TaimeTraquer::TaimeTraquer(QObject *parent, const QVariantList& args)
    : Plasma::AbstractRunner(parent, args)
    , m_trigger("tt")
    , m_gtimelogDir(QString("%1/.gtimelog").arg(QDir::homePath()))
    , m_gtimelogLog("timelog.txt")
{
    Q_UNUSED(args);
    setObjectName("TaimeTraquer");
    setIgnoredTypes( Plasma::RunnerContext::Directory
                     | Plasma::RunnerContext::Executable
                     | Plasma::RunnerContext::File
                     | Plasma::RunnerContext::FileSystem
                     | Plasma::RunnerContext::Help
                     | Plasma::RunnerContext::NetworkLocation
                     | Plasma::RunnerContext::ShellCommand
                   );

    m_deleteCacheTimer.setSingleShot(true);
    connect(&m_deleteCacheTimer, SIGNAL(timeout()), this, SLOT(clearCache()));
}

TaimeTraquer::~TaimeTraquer()
{
}


void TaimeTraquer::match(Plasma::RunnerContext &context)
{
    QString term = context.query();
    if (!term.startsWith(m_trigger)) {
        return;
    }

    QList<Plasma::QueryMatch> matches;

    if (term == QString("%1 %2").arg(m_trigger).arg("arrived")) {
        Plasma::QueryMatch match(this);
        match.setText(i18n("Arriving at Work"));
        match.setIcon(KIcon(KMimeType::iconNameForUrl(KUrl("Airplane"))));
        match.setRelevance(1.0);
        match.setType(Plasma::QueryMatch::ExactMatch);
        match.setId(QString::number(ARRIVED));
        matches << match;
    }
    else {
        QStringList words = term.split(QChar(' '));
        if (words.size() <= 1) {
            return;
        }

        // gTimeLog should record anything that ends with **, but shouldn't record just plain asterisk.
        if(words.size() == 2 && words.at(1).startsWith("**")) {
            return;
        }

        QString query = term.remove("tt ");

        // Create a 'record' match. this match will be used to append on the GTimeLog timelog.txt file in ~/.gtimelog/timelog.txt
        Plasma::QueryMatch recordMatch(this);
        recordMatch.setText(i18n("Record"));
        recordMatch.setIcon(KIcon(KMimeType::iconNameForUrl(KUrl("Record"))));
        recordMatch.setRelevance(1.0);
        recordMatch.setType(Plasma::QueryMatch::ExactMatch);
        recordMatch.setId(QString::number(RECORD));
        recordMatch.setData(query);
        matches << recordMatch;

        // Try to catch up all same ocorrences of the query for the same day, and display them.
        // gtimelog stores everything in a text file, so we need to parse everything always.
        // a timer will clear the cache 10 minutes after use, if the runner is not used anymore.
        if (!m_deleteCacheTimer.isActive()) {
            createCache();
        }
        
        // read the cache and populate the icons.
        Q_FOREACH(const QString& key, m_infoMap.keys()){
            qDebug() << key << query;
            if (key.startsWith(query)){
                const TaimeTraquerInfo& info = m_infoMap[key];
                if (info.m_date != QDate::currentDate()){
                    continue;
                }
                Plasma::QueryMatch cacheMatch(this);
                cacheMatch.setText(info.m_event);
                cacheMatch.setIcon(KIcon(KMimeType::iconNameForUrl(KUrl("Record"))));
                cacheMatch.setRelevance(0.9);
                cacheMatch.setType(Plasma::QueryMatch::InformationalMatch);
                cacheMatch.setId(QString::number(INFO));
                matches << cacheMatch;
            }
        }
        

    }

    context.addMatches(context.query(), matches);
}

void TaimeTraquer::run(const Plasma::RunnerContext &context, const Plasma::QueryMatch &match)
{
    Q_UNUSED(context)

    QDir::setCurrent(m_gtimelogDir.absolutePath());

    if (match.id().endsWith(QString::number(ARRIVED))) {
        record("arrived");
    }
    else if (match.id().endsWith(QString::number(RECORD))){
        record(match.data().toString());
    }
    return;
}

void TaimeTraquer::createRunOptions(QWidget* widget)
{
    Plasma::AbstractRunner::createRunOptions(widget);
}

void TaimeTraquer::init()
{
    reloadConviguration();
    connect(this, SIGNAL(prepare()),  this, SLOT(prepareForMatchSession()));
    connect(this, SIGNAL(teardown()), this, SLOT(matchSessionFinished()));
    
    if (!m_gtimelogDir.exists()) {
        m_gtimelogDir.mkdir(m_gtimelogDir.dirName());
    }
}

void TaimeTraquer::matchSessionFinished()
{

}

void TaimeTraquer::prepareForMatchSession()
{

}

void TaimeTraquer::reloadConviguration()
{

}

void TaimeTraquer::clearCache()
{

}

void TaimeTraquer::createCache()
{
    qDebug() << "Creating Cache.";
    QDir::setCurrent(m_gtimelogDir.absolutePath());
    if (!m_gtimelogLog.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    QTextStream reader(&m_gtimelogLog);
    QString buffer;
    do{
        buffer = reader.readLine();
        // ignore anything that didn't started today for low memory usage.
        QString date = buffer.split(QChar(' ')).first();
    
        // 0 - Date, if there's no date, just skip it.
        QDate dt = QDate::fromString(date, "yyyy-MM-dd");
        
        if (!dt.isValid()){
            qDebug() << date <<  "invalid date, skipping";
            continue;
        }
        
        // Date is valid. assume that everything else also is.
        QTime t = QTime::fromString(buffer.split(QChar(' ')).at(1), "hh:mm");
        
        TaimeTraquerInfo info = createInfo(dt, t, buffer);
        m_infoMap[info.m_event] = info;
        qDebug() << "Created" << info.m_event;
    }while(!reader.atEnd());
    m_gtimelogLog.close();
}

void TaimeTraquer::record(const QString& data)
{
        QDir::setCurrent(m_gtimelogDir.absolutePath());
        if (!m_gtimelogLog.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
            qDebug() << "Could not open file.";
            return;
        }

        QTextStream out(&m_gtimelogLog);
        QDate currentDate = QDateTime::currentDateTime().date();
        QTime currentTime = QDateTime::currentDateTime().time();
        
        QString appendLine = QString("%1 %2: %3\n")
                             .arg(currentDate.toString(QString("yyyy-MM-dd")))
                             .arg(currentTime.toString(QString("hh:mm")))
                             .arg(data);
        out << appendLine;
        
        TaimeTraquerInfo info = createInfo(currentDate, currentTime, data);
        m_infoMap[info.m_event] = info;
        qDebug() << "Adicionando" << info.m_event << "ao mapa.";
        
        m_gtimelogLog.close();
}

TaimeTraquerInfo TaimeTraquer::createInfo(QDate date, QTime time, const QString& data)
{
        TaimeTraquerInfo info;
        info.m_date = date;
        info.m_time = time;
        info.m_eventType = data.endsWith("***")        ? TaimeTraquerInfo::Hidden 
                            : data.endsWith("**")      ? TaimeTraquerInfo::NotWork
                            : data.endsWith("arrived") ? TaimeTraquerInfo::Arriving
                            : /* normal work */            TaimeTraquerInfo::Work;
        
        // Remove the length of a random-date-time
        QString result = data;
        result = result.remove(0, 18); //length of  "2012-06-10 13:11:"
        result = result.endsWith("***") ? result.remove("***")
                : result.endsWith("**") ? result.remove("**")
                : result;
                
        info.m_event = result;
        return info;
}


#include "taimetraquer.moc"
